#include <iostream>
#include <vector>

int gcd(int a, int b)
{
	for (;;)
	{
		if (a == 0) return b;
		b %= a;
		if (b == 0) return a;
		a %= b;
	}
}

int lcm(int a, int b)
{
	int temp = gcd(a, b);

	return temp ? (a / temp * b) : 0;
}

int add(int a, int b)
{
	return (a + b) % 7;
}

int sub(int a, int b)
{
	return ((a - b) + 7) % 7;
}


int times (int a, int b)
{
	return (a * b) % 7;
}

int divide(int a, int b)
{
	int lcm_ = lcm(a, b);
	int b_;
	int a_;

	if (a < b)
	{
		b_ = lcm_ + b;
		a_ = lcm_;
	}
	else
	{
		a_ = lcm_ + a;
		b_ = lcm_;
	}
	
	return (a_ / (b_ % 7)) % 7;
}

// GF(7) power of 5
int pow(int n)
{
	if (n == 0) return 1;
	if (n == 1) return 5;
	if (n == 2) return 4;
	if (n == 3) return 6;
	if (n == 4) return 2;
	if (n == 5) return 3;
	if (n == 6) return 1;
}

int main(int argc, char* argv[])
{
	//todo https://habr.com/ru/post/191418/
	using namespace std;
	auto code_word = vector<int>({ 3,1 });
	code_word.insert(code_word.end(), { 0,0,0,0 });

	//int a = (4 + 5) % 7;
	//int b = (30 / (36 % 7)) % 7;

	// �=3*�0+1*�1 + 0*�2 +� = 3+�

	// x^0 * cw[0] + cw[1] * 5^n
	int c0 = 3 + 1 * pow(0);
	int c1 = (3 + 1 * pow(1)) % 7;
	int c2 = (3 + 1 * pow(2)) % 7;
	int c3 = (3 + 1 * pow(3)) % 7;
	int c4 = 3 + 1 * pow(4);
	int c5 = 3 + 1 * pow(5);

	// �(4,1,0,2,5,6).
	// ������ f=(0,0,0,2,0,6).
	// � + f, �� �f(4,1,0,4,5,5).


	// DFT Decoding
	// (4 + 1 + 0 + 4 + 5 + 5)
	int c0_ = (4 * pow(0) + 1 * pow(0) + 0 + 4 * pow(0) + 5 * pow(0) + 5 * pow(0)) / 6;
	int c1_ = (4 *pow(0) )/6;
	int c2_ = (0)/6;
	
	return 0;
}
