

#include <iostream>
#include "rs.hpp"
using namespace std;

#define ECC_LENGTH 8

int main() {

    // char message[] = "Some very important message ought to be delivered";
    char message[] = "Hello world! Hello world! Hello world! Hello world! Hello world!";


    std::cout << "vvedite stroku \n";
    std::cin.getline(message, 250);
    std::cout << "\n";

	
    const int msglen = sizeof(message);

    char repaired[msglen];
    char encoded[msglen + ECC_LENGTH];


    RS::ReedSolomon<msglen, ECC_LENGTH> rs;

    rs.Encode(message, encoded);

    // Corrupting first 8 bytes of message (any 8 bytes can be repaired)
    // for (uint8_t i = 0; i < ECC_LENGTH / 2; i++) {
    //     encoded[i] = 'E';
    // }

    // for (uint8_t i = 5; i < (ECC_LENGTH / 2) + 5 ; i++) {
    //     encoded[i] = '0';
    // }


    encoded[0] = '0';
    encoded[1] = '0';
    encoded[2] = '0';
    encoded[3] = '0';
	

    rs.Decode(encoded, repaired);

    // std::cout << "Original:  " << message << std::endl;
    // std::cout << "Corrupted: " << encoded << std::endl;
    // std::cout << "Repaired:  " << repaired << std::endl;
    std::cout << "Message:  " << message << std::endl;
    std::cout << "Message corrupted: " << encoded << std::endl;
    std::cout << "Message repaired:  " << repaired << std::endl;

    // std::cout << ((memcmp(message, repaired, msglen) == 0) ? "SUCCESS" : "FAILURE") << std::endl;
    system("pause");
    return 0;
}