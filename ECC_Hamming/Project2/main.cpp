#include <iostream>
#include <string>
#include <vector>

using namespace std;


std::size_t divide_rounding_up(std::size_t dividend, std::size_t divisor)
{
	return (dividend + divisor - 1) / divisor;
}

std::string to_string(std::vector< bool > const& bitvector) {
	std::string ret(divide_rounding_up(bitvector.size(), 8), 0);
	auto out = ret.begin();
	int shift = 0;

	for (bool bit : bitvector) {
		*out |= bit << shift;

		if (++shift == 8) {
			++out;
			shift = 0;
		}
	}
	return ret;
}

int calculate_error_position(int n)
{
	switch (n)
	{
	case 3:
		return 1;
	case 5:
		return 2;
	case 6:
		return 3;
	case 7:
		return 4;
	case 8:
		return 5;
	case 9:
		return 6;
	case 10:
		return 7;
	default:
		return 0;
	}
}


void read_string(string& readed_string)
{
	//read from console
	cout << "Enter a string \n";
	cin >> readed_string;
}

void encoding(string& readed_string, vector<bool>& readed_bites, vector<bool>& original_bites)
{
	// convert string to vector<bool> ascii characters
	for (int i = 0; i < readed_string.size(); i++) {

		for (int j = 0; j < 7; j++)
		{
			original_bites.push_back(readed_string[i] & (1 << (6 - j)));
		}
	}

	for (auto original_bite : original_bites) cout << original_bite;
	cout << "\n";

	// convert string to vector<bool> ascii characters
	for (int i = 0; i < readed_string.size(); i++) {

		for (int j = 0; j < 7; j++)
		{
			// add placeholders for parity bits
			if (j == 0)
			{
				readed_bites.push_back(0);
				readed_bites.push_back(0);
			}
			if (j == 1)
			{
				readed_bites.push_back(0);				
			}
			if (j == 4)
			{
				readed_bites.push_back(0);
			}
			readed_bites.push_back(readed_string[i] & (1 << (6 - j)));
		}
    	
	}

	// add parity bits
	for (int i = 0; i < readed_bites.size(); i++)
	{
		// fill placeholders with parity bits
		if (i % 11 == 0)
		{
			int it = i;
			readed_bites[it + 0] = readed_bites[it + 2] ^ readed_bites[it + 4] ^ readed_bites[it + 6];
			readed_bites[it + 1] = readed_bites[it + 2] ^ readed_bites[it + 5] ^ readed_bites[it + 6];
			readed_bites[it + 3] = readed_bites[it + 4] ^ readed_bites[it + 5] ^ readed_bites[it + 6];
			readed_bites[it + 7] = readed_bites[it + 8] ^ readed_bites[it + 9] ^ readed_bites[it + 10];
		}
		
	}

	for (auto readed_bite : readed_bites) cout << readed_bite;
}

void add_errors(vector<bool>& readed_bites)
{
	// add error
	readed_bites[5] = !readed_bites[5];
	readed_bites[13] = !readed_bites[13];
	readed_bites[24] = !readed_bites[24];

	cout << "\n";
	for (auto bite : readed_bites) cout << bite;
	cout << "\n";
}

void decoding(vector<bool>& readed_bites, vector<bool>& decoded_bites)
{
	decoded_bites = vector<bool>();
	for (int i = 0; i < readed_bites.size(); i++)
	{
		// fill placeholders with parity bits
		if (i % 11 == 0)
		{
			int it = i;
			bool c1 = readed_bites[it + 0] ^ readed_bites[it + 2] ^ readed_bites[it + 4] ^ readed_bites[it + 6];
			bool c2 = readed_bites[it + 1] ^ readed_bites[it + 2] ^ readed_bites[it + 5] ^ readed_bites[it + 6];
			bool c3 = readed_bites[it + 3] ^ readed_bites[it + 4] ^ readed_bites[it + 5] ^ readed_bites[it + 6];
			bool c4 = readed_bites[it + 7] ^ readed_bites[it + 8] ^ readed_bites[it + 9] ^ readed_bites[it + 10];

			if ((c1 || c2 || c3 || c4) == 0)
			{
				for (int j = 0; j < 11; j++)
				{
					if (j != 0 & j != 1 & j != 3 & j != 7)
					{
						decoded_bites.push_back(readed_bites[j + it]);
					}
				}
			}
			else
			{
				// error in position
				int position = c1 * 1 + c2 * 2 + c3 * 4 + c4 * 8;
				int pos_ = calculate_error_position(position);
				int bites_writen = 0;

				for (int j = 0; j < 11; j++)
				{
					if ((j != 0) && (j != 1) && (j != 3) && (j != 7))
					{
						// error in parity bits
						if ((position & (position - 1)) == 0)
						{
							decoded_bites.push_back(readed_bites[j + it]);
							bites_writen++;
						}
						if (bites_writen == pos_ - 1)
						{
							decoded_bites.push_back(!readed_bites[j + it]);
							bites_writen++;
							continue;
						}
						else
						{
							decoded_bites.push_back(readed_bites[j + it]);
							bites_writen++;
						}
						
					}
				}
			}
		}
	}
	
	cout << "\n";
	for (auto decoded_bite : decoded_bites) cout << decoded_bite;
	cout << "\n";
}

void show_string(vector<bool>& decoded_bites)
{
	std::vector<bool> decoded_bites_8 = vector<bool>();
	for (int i = 0; i < decoded_bites.size(); i++)
	{
		if ((i % 7 == 0)) // && (i != 0)
		{
			decoded_bites_8.push_back(0);
			//continue;
		}
		decoded_bites_8.push_back(decoded_bites[i]);
	}
	
	std::string output_str;
	//output_str += to_string(decoded_bites_8);

	char c = 0;
	int shift = 0;

	for(int j = 0; j < decoded_bites_8.size(); j++)
	{
		if (j % 8 == 0)
		{
			int it = j;
			for (int i = 7; i > -1; i--)
			{
				c += (decoded_bites_8[i + j] << shift);
				shift++;
			}
			output_str += c;
			c = 0;
			shift = 0;
		}
	}
	
	cout << output_str;
}

int main(int argc, char* argv[])
{
	string readed_string;
	vector<bool> readed_bites = vector<bool>();
	vector<bool> original_bites = vector<bool>();
	
	read_string(readed_string);

	encoding(readed_string, readed_bites, original_bites);

	add_errors(readed_bites);

	vector<bool> decoded_bites;
	
	decoding(readed_bites, decoded_bites);


	show_string(decoded_bites);
	
	std::cout << "\n";
	system("pause");
	return 0;
}
