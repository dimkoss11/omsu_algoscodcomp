encode:

read from console
convert to ascii
convert ascii char to byte
add parity bits
store

decode:

load
remove parity bits
check parity bits
show warning
replace wrong bits
convert byte to ascii
write to console