#include <iostream>

using namespace std;


int main(int argc, char** argv) {

	int a[12];
	int b[12];
	int c1;
	int c2;
	int c3;
	int c4;

	//Data = 3 5 6 7 9 10 11
	//Parity = 1 2 4 8

	cout << "Enter 7 data bits pressing enter after each \n";
	
	cin >> a[3];
	cin >> a[5];
	cin >> a[6];
	cin >> a[7];
	cin >> a[9];
	cin >> a[10];
	cin >> a[11];


	// coding
	a[1] = a[3] ^ a[5] ^ a[7];
	a[2] = a[3] ^ a[6] ^ a[7];
	a[4] = a[5] ^ a[6] ^ a[7];
	a[8] = a[9] ^ a[10] ^ a[11];

	// https://habr.com/ru/post/140611/

	for (int i = 1; i < 12; i++)
	{
		cout << "\t" << a[i] << " ";
	}


	cout << " \n Enter 11 bits pressing enter after each \n";
	for (int i = 1; i < 12; i++)
	{
		cin >> b[i];
	}


	// decoding
	c1 = b[1] ^ b[3] ^ b[5] ^ b[7];
	c2 = b[2] ^ b[3] ^ b[6] ^ b[7];
	c3 = b[4] ^ b[5] ^ b[6] ^ b[7];
	c4 = b[8] ^ b[9] ^ b[10] ^ b[11];

	// error in position
	int position = c1 * 1 + c2 * 2 + c3 * 4 + c4 * 8;

	if (position == 0)
	{
		cout << " \n no errors";
	}
	else
	{
		cout << "\n error in " << position << "position \n";
		if (b[position] == 0) b[position] = 1;
		else b[position] = 0;

		for (int i = 1; i < 12; i++)
		{
			cout << "\t" << b[i];
		}
	}
	
    return 0;
}
