#pragma once
#include <windows.h>
#include <fileapi.h>
#include <Tchar.h>

typedef struct
{
	unsigned char application_id[3];
	unsigned char version[3];
}
application_extension_t;

typedef struct
{
	unsigned short width;
	unsigned short height;
	unsigned char fields;
	unsigned char background_color_index;
	unsigned char pixel_aspect_ratio;
}
screen_descriptor_t;

typedef struct
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
}
rgb;

#define EXTENSION_INTRODUCER   0x21
#define IMAGE_SEPARTOR 0x2C;
#define TRAILER                0x3B

typedef struct
{
	byte graphic_control_label;
	byte block_size;
	unsigned char fields;
	unsigned short delay_time;
	unsigned char transparent_color_index;
	byte block_terminator; // ??? byte image_separator;
}
graphic_control_extension_t;

#pragma pack (push, 1)
typedef struct
{
	unsigned short image_left_position;
	unsigned short image_top_position;
	unsigned short image_width;
	unsigned short image_height;
	unsigned char fields;
	// Local Color Table Flag        1 Bit
	// 	Interlace Flag                1 Bit
	// 	Sort Flag                     1 Bit
	// 	Reserved                      2 Bits
	// 	Size of Local Color Table     3 Bits
}
image_descriptor_t;
#pragma pack(pop)


HANDLE hIn, hOut;
DWORD RW;
DWORD OffBits;

application_extension_t appext;
screen_descriptor_t scrdesc;
rgb rgb_[256];
image_descriptor_t imgd;
byte extension_introducer;
graphic_control_extension_t gce;
byte lzw_min_code_size;

byte data_sub_block[256];



bool read_gif_()
{
	LPCWSTR fin = _T("sample.gif");
	hIn = CreateFile(fin, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hIn == INVALID_HANDLE_VALUE)
		return false;


	// read application extension
	ReadFile(hIn, &appext, sizeof(appext), &RW, NULL);

	// SetFilePointer(hIn, bfh.bfOffBits, NULL, FILE_BEGIN);

	// read screen descriptor
	ReadFile(hIn, &scrdesc, sizeof(scrdesc), &RW, NULL); // F7 11110111
	// 1 - Global color table
	// 111 - color res (7)
	// 0 - not sorted
	// 111 - size of color table (7)

	byte size_of_ct = (scrdesc.fields >> 5) & 0x7;
	
	// read global color table
	ReadFile(hIn, &rgb_, 767, &RW, NULL); //768

	// need check for GCE is present !!!
	ReadFile(hIn, &extension_introducer, 1, &RW, NULL);
	
	//read GCE
	ReadFile(hIn, &gce, sizeof(gce), &RW, NULL);
	// SetFilePointer(hIn, bfh.bfOffBits, NULL, FILE_BEGIN);

	// read image desc
	ReadFile(hIn, &imgd, sizeof(imgd), &RW, NULL);

	// maybe local color table is present


	// Table Based Image Data.

	// read LZW Minimum Code Size  
	ReadFile(hIn, &lzw_min_code_size, 1, &RW, NULL);

	//read data
	ReadFile(hIn, &data_sub_block, 256, &RW, NULL);

	
	return true;
}