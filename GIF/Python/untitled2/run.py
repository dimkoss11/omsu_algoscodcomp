import os
import imageio
import time
def create_gif():
    os.chdir('D:/code/algos/GIF/Python/untitled2/images')  # CHANGE HARDCODED PATH !!!!
    from PIL import Image, ImageDraw

    images = []

    width = 200
    center = width // 2
    color_1 = (0, 0, 0)
    color_2 = (255, 255, 255)
    max_radius = int(center * 1.5)
    step = 8

    for i in range(0, max_radius, step):
        im = Image.new('RGB', (width, width), color_1)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_2)
        images.append(im)

    for i in range(0, max_radius, step):
        im = Image.new('RGB', (width, width), color_2)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_1)
        images.append(im)

    images[0].save('pillow_imagedraw.gif',
                   save_all=True, append_images=images[1:], optimize=False, loop=0)

# create_gif()

def create_gif2():
    print(os.path.dirname(os.path.abspath(__file__)))
    print(os.path.abspath(os.getcwd()))
    # os.chdir('D:/code/algos/GIF/Python/untitled2/images') #CHANGE HARDCODED PATH !!!!
    gif_dir = '.'
    images = []
    for file_name in os.listdir(gif_dir):
        if file_name.endswith('.gif'):
            file_path = os.path.join(gif_dir, file_name)
            images.append(imageio.imread(file_path))
    imageio.mimsave('movie.gif', images, duration=[5, 10, 15, 15])
    time.sleep(5.5)
    print('created')

def change_duration():
    from PIL import Image
    f = Image.open('movie.gif')
    f.info['duration'] = [5000, 10000, 15000, 15000]
    f.save('out.gif', save_all=True)


create_gif2()

# change_duration()
