import os
# import imageio

def read_gif():
    print('Enter smth')
    beautiful_number = input()
    print('Hello')

# def create_gif():
#     print(os.path.dirname(os.path.abspath(__file__)))
#     print(os.path.abspath(os.getcwd()))
#     os.chdir('D:/code/algos/GIF/Python/untitled1/images') #CHANGE HARDCODED PATH !!!!
#     gif_dir = '.'
#     images = []
#     for file_name in os.listdir(gif_dir):
#         if file_name.endswith('.gif'):
#             file_path = os.path.join(gif_dir, file_name)
#             images.append(imageio.imread(file_path))
#     imageio.mimsave('movie.gif', images, duration=[0.5, 1.5, 3, 6])
#
#     print('created')

# read_gif()

def create_gif():
    os.chdir('D:/code/algos/GIF/Python/untitled1/images')  # CHANGE HARDCODED PATH !!!!
    from PIL import Image, ImageDraw

    images = []

    width = 200
    center = width // 2
    color_1 = (0, 0, 0)
    color_2 = (255, 255, 255)
    max_radius = int(center * 1.5)
    step = 8

    for i in range(0, max_radius, step):
        im = Image.new('RGB', (width, width), color_1)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_2)
        images.append(im)

    for i in range(0, max_radius, step):
        im = Image.new('RGB', (width, width), color_2)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_1)
        images.append(im)

    images[0].save('pillow_imagedraw.gif',
                   save_all=True, append_images=images[1:], optimize=False, duration=40, loop=0)

create_gif()


# from PIL import Image
# f = Image.open('test.gif')
# f.info['duration'] = [1000,100,200,200,200,200]
# f.save('out.gif', save_all=True)