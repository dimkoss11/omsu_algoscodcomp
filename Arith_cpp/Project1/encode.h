#pragma once
#include <map>
#include <vector>

#include "sorted.h"

double encode(std::string encode_str, int n_update)
{
	using namespace std;
	auto count = map<char, double>({ {'a',1}, {'b',1}, {'c',1}, {'d',1}, {'e',1}, {'f',1}, {'g',1}, {'h',1}, {'i',1}, {'j',1}, {'k',1}, {'l',1}, {'m',1}, {'n',1}, {'o',1}, {'p',1}, {'q',1}, {'r',1}, {'s',1}, {'t',1}, {'u',1}, {'v',1}, {'w',1}, {'x',1}, {'y',1}, {'z',1} });
	auto cdf_range = map<char, pair<double, double>>({ {'a', {0,0}}, {'b',{0,0}}, {'c',{0,0}}, {'d',{0,0}}, {'e',{0,0}}, {'f',{0,0}}, {'g',{0,0}}, {'h',{0,0}}, {'i',{0,0}}, {'j',{0,0}}, {'k',{0,0}}, {'l',{0,0}}, {'m',{0,0}}, {'n',{0,0}}, {'o',{0,0}}, {'p',{0,0}}, {'q',{0,0}}, {'r',{0,0}}, {'s',{0,0}}, {'t',{0,0}}, {'u',{0,0}}, {'v',{0,0}}, {'w',{0,0}}, {'x',{0,0}}, {'y',{0,0}}, {'z',{0,0}} });
	auto pdf = map<char, double>({ {'a', 0}, {'b', 0}, {'c', 0}, {'d', 0}, {'e', 0}, {'f', 0}, {'g', 0}, {'h', 0}, {'i', 0}, {'j', 0}, {'k', 0}, {'l', 0}, {'m', 0}, {'n', 0}, {'o', 0}, {'p', 0}, {'q', 0}, {'r', 0}, {'s', 0}, {'t', 0}, {'u', 0}, {'v', 0}, {'w', 0}, {'x', 0}, {'y', 0}, {'z', 0} });

	auto low = 0.0;
	auto high = 1.0 / 26;

	for (auto item: sorted(cdf_range))
	{
		cdf_range[item.first] = pair<double, double>(low, high);
		low = high;
		high += 1 / 26.0;
	}

	for(auto item: sorted(pdf) )
	{
		pdf[item.first] = 1 / 26.0;
	}

	auto i = 26;
	auto lower_bound = 0.0;
	auto upper_bound = 1.0;
	auto u = 0;

	for (auto symbol: encode_str)
	{
		i += 1;
		u += 1;
		count[symbol] += 1;

		auto curr_range = upper_bound - lower_bound;
		upper_bound = lower_bound + (curr_range * cdf_range[symbol].second);
		lower_bound = lower_bound + (curr_range * cdf_range[symbol].first);

		if (u == n_update)
		{
			u = 0;

			for (auto item: sorted(pdf))
			{
				pdf[item.first] = count[item.first] / double(i);
			}

			low = 0.0;

			for (auto item: sorted(cdf_range))
			{
				high = pdf[item.first] + low;
				cdf_range[item.first] = { low, high };
				low = high;
			}
			
		}
	}
	

	return lower_bound;
}
