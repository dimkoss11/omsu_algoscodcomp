#pragma once
#include "sorted.h"
#include <string>

std::string decode(double encoded, int strlen, int every)
{
	std::string decoded_str = "";
	
	using namespace std;
	auto count = map<char, double>({ {'a',1}, {'b',1}, {'c',1}, {'d',1}, {'e',1}, {'f',1}, {'g',1}, {'h',1}, {'i',1}, {'j',1}, {'k',1}, {'l',1}, {'m',1}, {'n',1}, {'o',1}, {'p',1}, {'q',1}, {'r',1}, {'s',1}, {'t',1}, {'u',1}, {'v',1}, {'w',1}, {'x',1}, {'y',1}, {'z',1} });
	auto cdf_range = map<char, pair<double, double>>({ {'a', {0,0}}, {'b',{0,0}}, {'c',{0,0}}, {'d',{0,0}}, {'e',{0,0}}, {'f',{0,0}}, {'g',{0,0}}, {'h',{0,0}}, {'i',{0,0}}, {'j',{0,0}}, {'k',{0,0}}, {'l',{0,0}}, {'m',{0,0}}, {'n',{0,0}}, {'o',{0,0}}, {'p',{0,0}}, {'q',{0,0}}, {'r',{0,0}}, {'s',{0,0}}, {'t',{0,0}}, {'u',{0,0}}, {'v',{0,0}}, {'w',{0,0}}, {'x',{0,0}}, {'y',{0,0}}, {'z',{0,0}} });
	auto pdf = map<char, double>({ {'a', 0}, {'b', 0}, {'c', 0}, {'d', 0}, {'e', 0}, {'f', 0}, {'g', 0}, {'h', 0}, {'i', 0}, {'j', 0}, {'k', 0}, {'l', 0}, {'m', 0}, {'n', 0}, {'o', 0}, {'p', 0}, {'q', 0}, {'r', 0}, {'s', 0}, {'t', 0}, {'u', 0}, {'v', 0}, {'w', 0}, {'x', 0}, {'y', 0}, {'z', 0} });

	auto low = 0.0;
	auto high = 1.0 / 26;

	for (auto item : sorted(cdf_range))
	{
		cdf_range[item.first] = {low, high};
		low = high;
		high += 1 / 26.0;
	}

	for (auto item : sorted(pdf))
	{
		pdf[item.first] = 1 / 26.0;
	}
	
	auto lower_bound = 0.0;
	auto upper_bound = 1.0;
	auto k = 0;

	while (strlen != decoded_str.size())
	{
		for (auto item : sorted(pdf))
		{
			auto curr_range = upper_bound - lower_bound;
			auto upper_cand = lower_bound + (curr_range * cdf_range[item.first].second);
			auto lower_cand = lower_bound + (curr_range * cdf_range[item.first].first);

			if (lower_cand <= encoded && encoded < upper_cand)
			{
				k += 1;
				decoded_str += item.first;
				
				if (strlen == decoded_str.size()) break;

				upper_bound = upper_cand;
				lower_bound = lower_cand;

				count[item.first] += 1;

				if (k == every)
				{
					k = 0;

					for (auto item: sorted(pdf))
					{
						pdf[item.first] = count[item.first] / (26 + decoded_str.size());
					}

					low = 0;

					for (auto item: sorted(cdf_range))
					{
						high = pdf[item.first] + low;
						cdf_range[item.first] = { low, high };
						low = high;
					}
				}
			}
		}
	}
	
	return decoded_str;
}