#pragma once
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
int main1() {

	std::map<std::string, int> mapOfWordCount = { {"aaa", 10}, {"ddd", 41}, {"bbb", 62}, {"ccc", 13} };

	typedef std::function<bool(std::pair<std::string, int>, std::pair<std::string, int>)> Comparator;

	Comparator compFunctor =
		[](std::pair<std::string, int> elem1, std::pair<std::string, int> elem2)
		{
			return elem1.second < elem2.second;
		};

	std::set<std::pair<std::string, int>, Comparator> setOfWords(
		mapOfWordCount.begin(), mapOfWordCount.end(), compFunctor);

	for (std::pair<std::string, int> element : setOfWords)
		std::cout << element.first << " " << element.second << std::endl;

	//
	// map<char, pair<double, double>> cdf_r;
	// cdf_r['v'] = { 3, 0 };
	// cdf_r['r'] = { 2, 0 };
	// cdf_r['a'] = { 1, 0 };
	//
	// vector<pair<char, pair<double, double>>> cdf_r_s;
	// copy(cdf_r.begin(),
	// 	cdf_r.end(),
	// 	back_inserter<vector<pair<char, pair<double, double> > >>(cdf_r_s));
	//
	// for (size_t i = 0; i < cdf_r_s.size(); ++i) {
	// 	cout << cdf_r_s[i].first << " , " << cdf_r_s[i].second.first << "\n";
	// }

	auto symbolTable = std::map<std::string, int>({{"aa", 1}});
	
	std::map<std::string, int>::iterator it;

	for (it = symbolTable.begin(); it != symbolTable.end(); it++)
	{
		std::cout << it->first  // string (key)
			<< ':'
			<< it->second   // string's value 
			<< std::endl;
	}

	for (auto const& x : symbolTable)
	{
		std::cout << x.first  // string (key)
			<< ':'
			<< x.second // string's value 
			<< std::endl;
	}

	// for (auto const& [key, val] : symbolTable)
	// {
	// 	std::cout << key         // string (key)
	// 		<< ':'
	// 		<< val        // string's value
	// 		<< std::endl;
	// }
	
	return 0;
}