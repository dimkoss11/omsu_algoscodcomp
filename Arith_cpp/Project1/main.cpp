#include <iostream>
#include <string>

#include "encode.h"
#include "decode.h"
using namespace std;

void str_from_console(std::string& str)
{
	std::cout << "enter a string \n";
	std::cin >> str;
}

int main(int argc, char* argv[])
{
	// auto encode_str = "sokolovdmitryiosivovic"s;
	auto encode_str = "helloworld"s;
	str_from_console(encode_str);

	
	const auto update_symbols = 3;
	const auto encoded = encode(encode_str, update_symbols);

	const auto strlen = encode_str.size();
	const auto decoded = decode(encoded, strlen, update_symbols);
	cout.precision(17);

	std::cout << "encoded string\n" << encoded << "\ndecoded string\n" << decoded;
	system("pause");
	return 0;
}
