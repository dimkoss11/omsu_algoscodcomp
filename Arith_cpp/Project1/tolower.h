#pragma once
#include <algorithm>
#include <string>

void tolower(std::string& encode_str)
{
	std::transform(encode_str.begin(), encode_str.end(), encode_str.begin(),
		[](unsigned char c) { return std::tolower(c); });
}
