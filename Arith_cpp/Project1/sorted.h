#pragma once

std::vector<std::pair<char, std::pair<double, double>>> sorted(std::map<char, std::pair<double, double>> cdf_range)
{
	std::vector<std::pair<char, std::pair<double, double>>> cdf_range_sorted;

	copy(cdf_range.begin(),
		cdf_range.end(),
		std::back_inserter<std::vector<std::pair<char, std::pair<double, double> > >>(cdf_range_sorted));

	return cdf_range_sorted;
}

std::vector<std::pair<char, double>> sorted(std::map<char, double> pdf)
{
	std::vector<std::pair<char, double>> pdf_sorted;

	copy(pdf.begin(),
		pdf.end(),
		std::back_inserter<std::vector<std::pair<char, double > >>(pdf_sorted));

	return pdf_sorted;
}