#include "pch.h"
#include "CppUnitTest.h"
#include <string>

#include "../Project1/main.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(Encoding)
	{
	public:
		
		TEST_METHOD(First)
		{
			std::string encode_str = "sokolovdmitryiosivovic";
			const auto update_symbols = 3;
			const auto encoded = encode(encode_str, update_symbols);
			
			Assert::AreEqual(0.7136178395443487, encoded, 0.00001);
		}

		TEST_METHOD(Second)
		{
			std::string encode_str = "helloworld";
			const auto update_symbols = 3;
			const auto encoded = encode(encode_str, update_symbols);
			Assert::AreEqual(0.27580170708612767, encoded, 0.00001);
		}
	};

	TEST_CLASS(Decoding)
	{
	public:

		TEST_METHOD(First)
		{
			std::string encode_str = "sokolovdmitryiosivovic";
			const auto update_symbols = 3;
			const auto encoded = encode(encode_str, update_symbols);
			
			const auto strlen = encode_str.size();
			const auto decoded = decode(encoded, strlen, update_symbols);
			Assert::AreEqual("sokolovdmitryiosivovic"s, decoded);
		}

		TEST_METHOD(Second)
		{
			std::string encode_str = "helloworld";
			const auto update_symbols = 3;
			const auto encoded = encode(encode_str, update_symbols);

			const auto strlen = encode_str.size();
			const auto decoded = decode(encoded, strlen, update_symbols);
			Assert::AreEqual("helloworld"s, decoded);
		}
	};
}
