#include <iostream>

#include "bmp.h"

#include "stdafx.h"

#include <fstream>

BOOL ConvertBMP256toASCII(const char* fin, const char* fout)
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;
	int Width, Height;
	RGBQUAD Palette[256];
	BYTE* inBuf;
	RGBTRIPLE* outBuf;
	HANDLE hIn, hOut;
	DWORD RW;
	DWORD OffBits;
	int i, j;

	hIn = CreateFile(fin, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hIn == INVALID_HANDLE_VALUE)
		return FALSE;

	// hOut = CreateFile(fout, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	// if (hOut == INVALID_HANDLE_VALUE)
	// {
	// 	CloseHandle(hIn);
	// 	return FALSE;
	// }

	// ������� ������
	ReadFile(hIn, &bfh, sizeof(bfh), &RW, NULL);
	ReadFile(hIn, &bih, sizeof(bih), &RW, NULL);
	ReadFile(hIn, Palette, 256 * sizeof(RGBQUAD), &RW, NULL);

	// ��������� ��������� �� ������ ������
	SetFilePointer(hIn, bfh.bfOffBits, NULL, FILE_BEGIN);

	Width = bih.biWidth;
	Height = bih.biHeight;
	OffBits = bfh.bfOffBits;

	// ������� ������
	inBuf = new BYTE[Width];
	outBuf = new RGBTRIPLE[Width];

	// �������� ���������
	bfh.bfOffBits = sizeof(bfh) + sizeof(bih);	// �� ����� ������ �������
	bih.biBitCount = 24;
	bfh.bfSize = bfh.bfOffBits + 4 * Width * Height + Height * (Width % 4);		// ������ �����
	// � ��������� �� ��������

	// ������� ���������
	// WriteFile(hOut, &bfh, sizeof(bfh), &RW, NULL);
	// WriteFile(hOut, &bih, sizeof(bih), &RW, NULL);

	// ��������
	BYTE* picBuf = new BYTE[Width * Height];

	// ������ ���������������
	for (int i = 0; i < Height; i++)
	{
		ReadFile(hIn, inBuf, Width, &RW, NULL);
		std::memcpy(picBuf + (i * Width), inBuf ,  Width);

		
		for (int j = 0; j < Width; j++)
		{
			outBuf[j].rgbtRed = Palette[inBuf[j]].rgbRed;
			outBuf[j].rgbtGreen = Palette[inBuf[j]].rgbGreen;
			outBuf[j].rgbtBlue = Palette[inBuf[j]].rgbBlue;

			// double lum = (0.2126 * Palette[inBuf[j]].rgbRed + 0.7152 * Palette[inBuf[j]].rgbGreen + 0.0722 * Palette[inBuf[j]].rgbBlue);
			// if (lum < 64)
			// {
			// 	std::cout << 1;
			// 	continue;
			// }
			// if (lum < 128)
			// {
			// 	std::cout << 7;
			// 	continue;
			// }
			// if (lum < 192)
			// {
			// 	std::cout << 5;
			// 	continue;
			// }
			// else
			// {
			// 	std::cout << 8;
			// }

		}

		std::cout << "\n";
		
		// WriteFile(hOut, outBuf, sizeof(RGBTRIPLE) * Width, &RW, NULL);
		
		// ����� ��� ������������
		// WriteFile(hOut, Palette, Width % 4, &RW, NULL);
		// SetFilePointer(hIn, (3 * Width) % 4, NULL, FILE_CURRENT);
	}

	// ������ txt �����
	std::ofstream myfile;
	myfile.open(fout);


	for (int i = Height; i > 0; i--)
	{
		for (int j = 0; j < Width; j++)
		{
			double lum = (0.2126 * Palette[picBuf[i * Width + j]].rgbRed + 0.7152 * Palette[picBuf[i * Width + j]].rgbGreen + 0.0722 * Palette[picBuf[i * Width + j]].rgbBlue);
			if (lum < 64)
			{
				std::cout << 1;
				myfile << 1;
				continue;
			}
			if (lum < 128)
			{
				std::cout << 7;
				myfile << 7;
				continue;
			}
			if (lum < 192)
			{
				std::cout << 5;
				myfile << 5;
				continue;
			}
			else
			{
				std::cout << 8;
				myfile << 8;
			}
		}
		std::cout << "\n";
		myfile << "\n";
	}

	myfile.close();

	delete inBuf;
	delete outBuf;
	CloseHandle(hIn);
	// CloseHandle(hOut);

	return TRUE;
}