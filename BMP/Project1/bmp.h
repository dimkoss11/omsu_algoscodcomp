#pragma once
#include "stdafx.h"

BOOL Convert256To24(const char* fin, const char* fout);
BOOL ConvertBMP256toASCII(const char* fin, const char* fout);
void drawToConsole(RGBQUAD Palette[], int j, BYTE* inBuf);