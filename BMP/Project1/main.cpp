#include <iostream>
#include "bmp.h"

using namespace std;

int main(int argc, char* argv[])
{
	
	// Convert256To24("D:/code/algos/BMP/Project1/Lenna.bmp", "D:/code/algos/BMP/Project1/Lenna_out.bmp");
	// ConvertBMP256toASCII("D:/code/algos/BMP/Project1/Lenna_s.bmp", "D:/code/algos/BMP/Project1/Lenna_out.bmp");

	char* filename = new char[20];
	const char* filename_out = "out.txt";
	std::cout << "please enter a filename: \n";
	std::cin >> filename;

	if (!strncmp(filename, "666",3))
	{
		auto str = "Lenna_s.bmp";
		filename = const_cast<char*>(str);
	}
	
	ConvertBMP256toASCII(filename, filename_out);
	system("pause");
	return 0;
}
