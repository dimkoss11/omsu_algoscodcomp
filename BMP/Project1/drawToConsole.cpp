#include <iostream>

#include "bmp.h"

void drawToConsole(RGBQUAD Palette[], int j, BYTE* inBuf)
{
	double lum = (0.2126 * Palette[inBuf[j]].rgbRed + 0.7152 * Palette[inBuf[j]].rgbGreen + 0.0722 * Palette[inBuf[j]].rgbBlue);
	if (lum < 64)
	{
		std::cout << 1;
		return;
	}
	if (lum < 128)
	{
		std::cout << 7;
		return;
	}
	if (lum < 192)
	{
		std::cout << 5;
		return;
	}
	else
	{
		std::cout << 8;
	}
}