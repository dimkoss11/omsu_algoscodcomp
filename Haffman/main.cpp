#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <queue>
using namespace std;

string encodedString = "";
map<char, string> codes;

/// �������� ������� ����
map<char, int> freq_map(string _input_word) {
	auto m = new map<char, int>();

	for (char item : _input_word) {
		int old_value = 0;
		old_value += (*m)[item];
		(*m)[item] = old_value + 1;
	}
	return *m;
}



struct Node {
	char char_;
	int freq;
	Node* left = NULL;
	Node* right = NULL;;

	Node(char char_, int freq) : char_(char_), freq(freq) {}

};

/// ��������� ������ ����
class compare {
public:
	bool operator()(Node* left, Node* right) {
		return left->freq > right->freq;
	}
};


void show_codes(Node* node, string sym) {
	if (!node)
		return;

	if (node->char_ != '_')
	{
		cout << node->char_ << ": " << sym << "\n";
	}

	show_codes(node->left, sym + "0");
	show_codes(node->right, sym + "1");
}

void store_codes(Node* node, string sym)
{
	if (node == NULL)
		return;
	if (node->char_ != '_')
		codes[node->char_] = sym;
	store_codes(node->left, sym + "0");
	store_codes(node->right, sym + "1");
}



priority_queue<Node*, vector<Node*>, compare> encoding(vector<char>* arr, vector<int>* freq) {
	 auto size = arr->size();

	 Node* left;
	 Node* right;
	 Node *top;

	priority_queue<Node*, vector<Node*>, compare> tree;

	for (int i = 0; i < size; ++i)
		tree.push(new Node(arr->at(i), freq->at(i)));

	while (tree.size() != 1) {

		left = tree.top();
		tree.pop();
		right = tree.top();
		tree.pop();
		
		top = new Node('_', left->freq + right->freq);

		top->left = left;
		top->right = right;

		tree.push(top);
	}

	show_codes(tree.top(), "");
	store_codes(tree.top(), "");
	return tree;
}

string decododing(Node* node, string str)
{
	string decoded_words = "";
	Node* current = node;
	for (int i = 0; i < str.size(); i++)
	{
		if (str[i] == '1')
			current = current->right;
		else
			current = current->left;

		if (current->left == NULL & current->right == NULL)
		{
			decoded_words += current->char_;
			current = node;
		}
	}
	return decoded_words;
	
}


int main() {

	//string _input_word = "my name is dima my second name is sokolov";
	string _input_word = "abracadabra";
	std::cout << "vvedite slovo \n";
	std::cin >> _input_word;

	auto arr = new vector<char>();
	auto freq = new vector<int>();
	map<char, int> m = freq_map(_input_word);

	for (auto item : m) {
		arr->push_back(item.first);
		freq->push_back(item.second);
	}
	
	//auto tree = encoding(arr, freq);

	// std::vector<char> arr1 = { 'a', 'b', 'c', 'd', 'e', 'f' };
	// std::vector<int> freq1  = { 5, 9, 12, 13, 16, 45 };
	auto tree = encoding(arr, freq);

	// string encodedString = "01101110100010101101110";


	std::cout << " \n ";
	
	for (auto item : _input_word)
	{
		std::cout << codes[item];
		encodedString = encodedString + codes[item];
	}

	string decodedString = decododing(tree.top(), encodedString);
	
	std::cout << " \n decodirovannaya stroka \n";

	std::cout << decodedString;
	std::cout << "\n";
	system("pause");
	
	return 0;
}